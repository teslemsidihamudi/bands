/*jshint node:true */
'use strict';

module.exports = function (grunt) {

    grunt.task.registerTask('indexPortal', '', function() {
      var fs = require('fs');

      var appName = grunt.config('name');

      var css = '<link rel="stylesheet" href="./styles/' + appName + '.min.css" />\n';
      var js = '<script src="./scripts/vendor.min.js"></script>\n<script src="./scripts/' + appName + '.min.js" ></script>\n';

      fs.writeFileSync('dist/index.html','<html>\n<head><meta charset="utf-8">\n' + css
      + '<div id="' + appName +'" ui-view autoscroll="false"></div>' + js
      + '<script>var element = document.getElementById("' + appName + '");angular.element(document).ready(function() {'
      + 'angular.bootstrap(element, [\'App\']);});</script>' +'</head>\n<body></body>\n</html>\n');
    });
};
