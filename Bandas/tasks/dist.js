'use strict';

module.exports = function (grunt) {
  grunt.registerTask('dist', [
    'clean:dist',
    'wiredep',
    'useminPrepare',
    'concurrent:dist',
    'postcss',
    'ngtemplates',
    'concat',
    'ngAnnotate',
    'copy:dist',
    'cdnify',
    'cssmin',
    'uglify',
    // 'filerev',
    // 'usemin',
    'htmlmin',
    'compress',
    'indexPortal'
  ]);
};
