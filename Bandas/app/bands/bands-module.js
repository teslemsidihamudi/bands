'use strict';

(function() {
    angular.module('App.bandsModule', [
        'videosharing-embed',
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ui.router',
        'videosharing-embed',
        'ui.router.metatags'
    ]);
})();