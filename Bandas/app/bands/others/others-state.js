'use strict';

(function() {
    angular.module('App.bandsModule')
        .config(['$stateProvider', function($stateProvider) {

            $stateProvider
                .state('foo', {
                    url: '/foo',
                    templateUrl: 'bands/others/others-tpl.html',
                    controller: 'othersController'
                })
                .state('bar', {
                    url: '/bar',
                    templateUrl: 'bands/others/others-tpl.html',
                    controller: 'othersController'
                });
        }]);
})();