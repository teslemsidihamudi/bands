'use strict';

(function() {
    angular.module('App.bandsModule')
        .config(['$stateProvider', function($stateProvider) {

            $stateProvider.state('home', {
                url: '/home',
                templateUrl: 'bands/home/home-tpl.html',
                controller: 'homeController'
            });
        }]);
})();