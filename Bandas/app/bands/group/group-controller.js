'use strict';

(function() {
    angular.module('App.bandsModule')
        .controller('groupController', homeController);

    homeController.$inject = ['$scope', 'data'];

    function homeController($scope, data) {

        function init() {
            $scope.model = {
                groupData: data
            };
        }

        init();
    }
})();