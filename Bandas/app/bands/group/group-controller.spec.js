'use strict'

describe('Unit: GroupController', function() {
    var scope, dataGroup;

    beforeEach(angular.mock.module('App'));
    beforeEach(angular.mock.module('App.bandsModule'));

    beforeEach(angular.mock.inject(function($controller, $rootScope) {
        scope = $rootScope.$new();
        dataGroup = {};

        $controller('homeController', {
            $scope: scope,
            data: dataGroup
        });
    }));

    it('$scope should exists', function() {
        expect(scope)
            .toBeDefined();
        expect(scope)
            .not.toBeNull();
        expect(scope)
            .toBeTruthy();
    });
});