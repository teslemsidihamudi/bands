'use strict';

(function() {
    angular.module('App.bandsModule')
        .controller('commonController', commonController);

    commonController.$inject = ['$scope', '$state', '$location', '$anchorScroll'];

    function commonController($scope, $state, $location, $anchorScroll) {

        function goToState(data) {
            $state.go(data);

            $location.hash('scrollArea');
            $anchorScroll();
        }

        function init() {
            $scope.goToState = goToState;
        }

        init();
    }
})();