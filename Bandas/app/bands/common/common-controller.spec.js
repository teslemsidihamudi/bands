'use strict'

describe('Unit: CommonController', function() {
    var scope, dataGroup, state;

    beforeEach(angular.mock.module('App'));
    beforeEach(angular.mock.module('App.bandsModule'));

    beforeEach(angular.mock.inject(function($controller, $rootScope, $state) {
        scope = $rootScope.$new();
        state = $state;
        dataGroup = {};

        spyOn(state, 'go');

        $controller('commonController', {
            $scope: scope,
            data: dataGroup
        });
    }));

    it('$scope should exists', function() {
        expect(scope)
            .toBeDefined();
        expect(scope)
            .not.toBeNull();
        expect(scope)
            .toBeTruthy();
    });

    it('scope.goToState should exists', function() {
        expect(scope.goToState)
            .toBeDefined();
        expect(scope.goToState)
            .not.toBeNull();
        expect(scope.goToState)
            .toBeTruthy();
        expect(typeof scope.goToState)
            .toEqual('function');
    });
    it('scope.goToState should work fine', function() {
        var data = 'queen';

        scope.goToState(data);
        expect(state.go);
    });
});