(function() {
    'use strict';

    angular.module('App.bandsModule')
        .factory('bandsDataFactory', bandsDataFactory);

    function getRollingsData() {
        var rollingData = {
            name: 'The Rolling Stones',
            video: 'https://www.youtube.com/watch?v=nrIPxlFzDi0',
            songTitle: 'I Can not Get No',
            images: [
                '/images/rolling1.jpg',
                '/images/rolling1.jpg',
                '/images/rolling1.jpg'
            ],
            singleImage: '/images/rolling2.jpg',
            description: 'The Rolling Stones are an English rock band formed in London in 1962. The original line-up consisted of Brian Jones (rhythm guitar, harmonica), Mick Jagger (lead vocals), Keith Richards (lead guitar, backing vocals), Bill Wyman (bass), Charlie Watts (drums), and Ian Stewart (piano). Stewart was removed from the official line-up in 1963 but continued as a touring member until his death in 1985. Jones left the band less than a month prior to his death in 1969, having already been replaced by Mick Taylor, who remained until 1974. After Taylor left the band, Ronnie Wood took his place in 1975 and has been on guitar in tandem with Richards ever since. Following Wyman',
            important: 'Tan pronto como comienzas a creerte importante, te vuelves menos creativo'
        };

        return rollingData;
    }

    function getBeatlesData() {
        var rollingData = {
            name: 'The Beatles',
            video: 'https://www.youtube.com/watch?v=Qyclqo_AV2M&list=PLmo4pBukfRoN8SB5RKvfiY9CTl9pI_IFc',
            songTitle: 'We Can Work it Out',
            images: [
                '/images/beatles1.jpg',
                '/images/beatles2.jpg',
                '/images/beatles3.jpg'
            ],
            singleImage: '/images/rolling2.jpg',
            description: 'The Beatles fue una banda de pop/rock inglesa activa durante la década de 1960, y reconocida como la más exitosa comercialmente y la más alabada por la crítica en la historia de la música popular.1​2​3​4​5​ Formada en Liverpool, estuvo constituida desde 1962 por John Lennon (guitarra rítmica, vocalista), Paul McCartney (bajo, vocalista), George Harrison (guitarra solista, vocalista) y Ringo Starr (batería, vocalista). Enraizada en el skiffle y el rock and roll de los años 1950, la banda trabajó más tarde con distintos géneros musicales, que iban desde las baladas pop hasta el rock psicodélico, incorporando a menudo elementos clásicos, entre otros, de forma innovadora en sus canciones. La naturaleza de su enorme popularidad, que había emergido primeramente con la moda de la «Beatlemanía», se transformó al tiempo que sus composiciones se volvieron más sofisticadas. Llegaron a ser percibidos como la encarnación de los ideales progresistas, extendiendo su influencia en las revoluciones sociales y culturales de la década de 1960.',
            important: 'Tan pronto como comienzas a creerte importante, te vuelves menos creativo'
        };

        return rollingData;
    }

    function getQueenData() {
        var rollingData = {
            name: 'Queen',
            video: 'https://www.youtube.com/watch?v=f4Mc-NYPHaQ',
            songTitle: 'I Want To Break Free',
            images: [
                '/images/queen.jpg',
                '/images/queen1.jpg',
                '/images/queen1.jpg'
            ],
            singleImage: '/images/queen-11.jpg',
            description: 'Queen es una banda británica de rock formada en 1970 en Londres por el cantante Freddie Mercury, el guitarrista Brian May, el baterista Roger Taylor y el bajista John Deacon. Si bien el grupo ha presentado bajas de dos de sus miembros (Mercury, fallecido en 1991, y Deacon, retirado en 1997), los integrantes restantes, May y Taylor, continúan trabajando bajo el nombre Queen, por lo que la banda aún es considerada activa',
            important: 'No seré una estrella de rock. Seré una leyenda'
        };

        return rollingData;
    }

    function bandsDataFactory() {
        var factory = {
            getRollingsData: getRollingsData,
            getBeatlesData: getBeatlesData,
            getQueenData: getQueenData
        };

        return factory;
    }
}());