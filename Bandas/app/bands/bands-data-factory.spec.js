'use strict'

describe('Unit: Bands Component :: bandsDataFactory', function() {
    var _bandsDataFactory;

    beforeEach(angular.mock.module('App'));
    beforeEach(angular.mock.module('App.bandsModule'));

    beforeEach(angular.mock.inject(function(bandsDataFactory) {
        _bandsDataFactory = bandsDataFactory;
    }));

    it('bandsDataFactory has nice rigth methods', function() {
        expect(_bandsDataFactory.getRollingsData)
            .toBeTruthy();
        expect(_bandsDataFactory.getBeatlesData)
            .toBeTruthy();
        expect(_bandsDataFactory.getQueenData)
            .toBeTruthy();
    });
});