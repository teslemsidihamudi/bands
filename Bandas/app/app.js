'use strict';

/**
 * @ngdoc overview
 * @name bands
 * @description
 * # bands
 *
 * Main module of the application.
 */

function runBlock($rootScope, MetaTags) {
    $rootScope.MetaTags = MetaTags;
}

function configure(UIRouterMetatagsProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/home');

    UIRouterMetatagsProvider
        .setTitlePrefix('Bands - ')
        .setTitleSuffix(' | Bands')
        .setDefaultTitle('Angular Rocks')
        .setDefaultDescription('Esto es la descripción por defecto')
        .setDefaultKeywords('angular rocks default')
        .setStaticProperties({
            'fb:app_id': 'Bands',
            'og:site_name': 'Bands'
        })
        .setOGURL(true);
}
angular
    .module('App', [
        'App.bandsModule'
    ])
    .config(['UIRouterMetatagsProvider', '$urlRouterProvider', configure])
    .run(['$rootScope', 'MetaTags', runBlock]);