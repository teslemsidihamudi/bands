// Generated on 2017-01-02 using generator-angular 0.15.1
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

  grunt.loadTasks('./tasks');

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Automatically load required Grunt tasks
  require('jit-grunt')(grunt, {
    useminPrepare: 'grunt-usemin',
    ngtemplates: 'grunt-angular-templates',
    cdnify: 'grunt-google-cdn'
  });

  // Configurable paths for the application
  var appConfig = {
    app: require('./bower.json').appPath || 'app',
    dist: 'dist',
    name: require('./bower.json').name || 'noName',
    version: require('./bower.json').version || '0.0.1',
    config: {
      src: ['config/*.js']
    }
  };

  // Load grunt configurations automatically at config folder
  var config = require('load-grunt-configs')(grunt, appConfig);
  config.yeoman = appConfig;


  // Define the configuration for all the tasks
  grunt.initConfig(config);

  grunt.registerTask('default', [
    'server'
  ]);
};
