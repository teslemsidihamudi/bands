'use strict';

// The following *-min tasks will produce minified files in the dist folder
// By default, your `index.html`'s <!-- Usemin block --> will take care of
// minification. These next options are pre-configured if you do not wish
// to use the Usemin blocks.
module.exports = {
  dist: {
    files: {
      '<%= yeoman.dist %>/styles/<%= yeoman.name %>.min.css': [
        '.tmp/styles/{,*/}*.css'
      ]
    }
  }
};
