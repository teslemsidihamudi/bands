'use strict';

module.exports = {
  dist: {
    options: {
      module: 'App',
      htmlmin: '<%= htmlmin.dist.options %>',
      usemin: 'scripts/scripts.js'
    },
    cwd: '<%= yeoman.app %>',
    src: '{,*/}*.html',
    dest: '.tmp/templateCache.js'
  }
};
