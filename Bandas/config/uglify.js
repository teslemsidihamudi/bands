'use strict';

module.exports = {
  options: {
      mangle: false,
      sourceMap: true
  },
  dist: {
    files: {
      '<%= yeoman.dist %>/scripts/<%= yeoman.name %>.min.js': [
        '.tmp/concat/scripts/scripts.js'],
        '<%= yeoman.dist %>/scripts/vendor.min.js': [
          '.tmp/concat/scripts/vendor.js']
    }
  }
};
