'use strict';

module.exports = {
  main: {
    options: {
        archive: '<%= yeoman.name %>' + '-' + '<%= yeoman.version %>' + '.tar.gz',
        mode: 'tgz'
    },
    files: [{
        expand: true,
        src: ['bower.json', 'package.json', 'README.md','./<%= yeoman.dist %>/'],
        cwd: '.',
        dest: ''
    },
   {
     expand: true,
     cwd: '<%= yeoman.dist %>',
     src: ['**'],
     dest: './'
   }
 ]
}
};
