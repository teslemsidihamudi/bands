'use strict';

module.exports = {
  options: {
    basePath: '<%= yeoman.app %>',
    baseUrl: '',
    ordering: 'top-down'
  },
  app: {
      files: {
          '<%= yeoman.app %>/index.html': '<%= yeoman.app %>/index.html'
          // you can add karma config as well here if want inject to karma as well
      }
  }
};
